﻿using System;
using System.Collections;
using System.Linq;


namespace Task2
{
    internal static class Task
    {
        public static void RunTask()
        {
            /*
              LinqBegin1. Дана целочисленная последовательность, содержащая как положительные, так и отрицательные числа. 
              Вывести ее первый положительный элемент и последний отрицательный элемент.
             */
            var intArray1 = new int[10] {-5, -7, 10, 1, -9, 9, -15, 11, 0, 5 };
            var result1 = intArray1.Where(a => a > 0).Take(1).Union(intArray1.Reverse().Where(a => a<0).Take(1));
            Console.WriteLine("\nLinqBegin1");
            Input(result1);

            /*
             LinqBegin2. Дана цифра D (однозначное целое число) и целочисленная последовательность A. 
             Вывести первый положительный элемент последовательности A, оканчивающийся
             цифрой D. Если требуемых элементов в последовательности A нет, то вывести 0.
             */
            var d = 7;
            var intArray21 = new int[10] { -5, -7, 10, 0, -9, 9, -15, 17, 0, 5 };
            var intArray22 = new int[10] { -5, -7, 10, 0, -9, 9, -15, 11, 0, 5 };
            var result21 = intArray21.Where(a => (a - d) % 10 == 0 && a > 0).FirstOrDefault();
            var result22 = intArray22.Where(a => (a - d) % 10 == 0 && a > 0).FirstOrDefault();
            Console.WriteLine("\nLinqBegin2");
            Console.WriteLine(result21);
            Console.WriteLine(result22);

            /*
             LinqBegin3. Дано целое число L (> 0) и строковая последовательность A. 
             Вывести последнюю строку из A, начинающуюся с цифры и имеющую длину L. 
             Если требуемых строк в последовательности A нет, то вывести строку «Not found».
            Указание. Для обработки ситуации, связанной с отсутствием требуемых строк, использовать операцию ??.
             */
            var l = 5;
            var stringArray31 = new string[5] {"asdfg", "qwert", "3aa", "9abcd", "cccccccc"};
            var stringArray32 = new string[5] {"asdfg", "qwert", "3aa", "9abcde", "cccccccc" };
            var result31 = stringArray31.FirstOrDefault(s => s.Length == l && s.Substring(0,1).Intersect("0123456789").Any()) ?? "Not found";
            var result32 = stringArray32.FirstOrDefault(s => s.Length == l && s.Substring(0,1).Intersect("0123456789").Any()) ?? "Not found";
            Console.WriteLine("\nLinqBegin3");
            Console.WriteLine(result31);
            Console.WriteLine(result32);

            /*
             LinqBegin4. Дан символ С и строковая последовательность A. Если A содержит единственный элемент, оканчивающийся символом C, 
             то вывести этот элемент; если требуемых строк в A нет, то вывести пустую строку; если требуемых строк больше одной, 
             то вывести строку «Error». Указание. Использовать try-блок для перехвата возможного исключения.
             */
            var ch = 'c';
            var stringArray41 = new string[5] { "asdfg", "qwert", "3aa", "9abcd", "0" };
            var stringArray42 = new string[5] { "asdfg", "qwert", "3aa", "9abcd", "cccccccc" };
            var stringArray43 = new string[5] { "asdfg", "qwert", "3aa", "9abcdc","cccccccc" };
            
            Console.WriteLine("\nLinqBegin4");
            try
            {
                var result41 = stringArray41.Where(s => s.Last() == ch);
                var enumerable1 = result41 as string[] ?? result41.ToArray();
                if (!enumerable1.Any())
                    Console.WriteLine("");
                else 
                    if (enumerable1.Count() == 1)
                    Console.WriteLine(enumerable1.ElementAt(0));
                else throw new Exception();
                var result42 = stringArray42.Where(s => s.Last() == ch);
                var enumerable2 = result42 as string[] ?? result42.ToArray();
                if (!enumerable2.Any())
                    Console.WriteLine("");
                else
                    if (enumerable2.Count() == 1)
                    Console.WriteLine(enumerable2.ElementAt(0));
                else throw new Exception();
                var result43 = stringArray43.Where(s => s.Last() == ch);
                var enumerable3 = result41 as string[] ?? result43.ToArray();
                if (!enumerable3.Any())
                    Console.WriteLine("");
                else
                    if (enumerable3.Count() == 1)
                    Console.WriteLine(enumerable3.ElementAt(0));
                else throw new Exception();
            }
            catch
            {
                Console.WriteLine("Error");
            }

            /*
             LinqBegin5. Дан символ С и строковая последовательность A. Найти количество элементов A, которые содержат более одного символа 
             и при этом начинаются и оканчиваются символом C.
             */
            var ch5 = 'a';
            var stringArray5 = new string[8] { "asdfg", "aasda", "qwert", "3aa", "9abcd", "a5a", "ccccccc", "a" };
            var result5 = stringArray5.Count(s => s.Length > 1 && s[0] == ch5 && s[s.Count()-1] == ch5); 
            Console.WriteLine("\nLinqBegin5");
            Console.WriteLine(result5);

            /*
             LinqBegin6. Дана строковая последовательность. Найти сумму длин всех строк, входящих в данную последовательность. 
             */
            var stringArray6 = new string[8] { "asdfg", "aasda", "qwert", "3aa", "9abcd", "a5a", "ccccccc", "a" };
            var result6 = 0;
            result6 = stringArray6.Select(s => s.Length).Sum();
            Console.WriteLine("\nLinqBegin6");
            Console.WriteLine(result6);

            /*
             LinqBegin7. Дана целочисленная последовательность. Найти количество ее отрицательных элементов, а также их сумму. 
             Если отрицательные элементы отсутствуют, то дважды вывести 0.
             */
            var intArray71 = new int[10] { -5, -7, 10, 0, -9, 9, -15, 17, 0, 5 };
            var result71 = intArray71.Where(i => i < 0);
            var intArray72 = new int[10] { 5, 7, 10, 0, 9, 9, 15, 17, 0, 5 };
            var result72 = intArray72.Where(i => i < 0);
            Console.WriteLine("\nLinqBegin7");
            var enumerable = result71 as int[] ?? result71.ToArray();
            Console.WriteLine("Количество: {0}  Сумма: {1}", enumerable.Count(), enumerable.Sum());
            var ints = result72 as int[] ?? result72.ToArray();
            Console.WriteLine("Количество: {0}  Сумма: {1}", ints.Count(), ints.Sum());

            /*
             * LinqBegin12. Дана целочисленная последовательность. Используя метод Aggregate, найти произведение последних цифр всех элементов последовательности. 
             * Чтобы избежать целочисленного переполнения, при вычислении произведения использовать вещественный числовой тип.
             */
            var intArray12 = new int[10] { -5462, -7562451, 12562, 25622, -2561, 9542, -124561, 12562, 255, 525251 };
            var result12 = intArray12.Aggregate(1, (x, y) => x * int.Parse(y.ToString().Substring(y.ToString().Count()- 1, 1)));
            Console.WriteLine("\nLinqBegin12");
            Console.WriteLine(result12);

            /*
             LinqBegin13. Дано целое число N (> 0). Используя методы Range и Sum, найти сумму 1 + (1/2) + … + (1/N) (как вещест-венное число).
             */
            var i13 = 5;
            var result13 = Enumerable.Range(1, i13).Sum(a => (double)1/a);
            Console.WriteLine("\nLinqBegin13");
            Console.WriteLine(result13);

            /*
             LinqBegin15. Дано целое число N (0 ≤ N ≤ 15). Используя методы Range и Aggregate, найти факториал числа N: N! = 1·2·…·N при N ≥ 1; 0! = 1. 
             Чтобы избежать целочислен-ного переполнения, при вычислении факториала использовать вещественный числовой тип.
             */
            var i15 = 5;
            var result15 = Enumerable.Range(1, i15).Aggregate(1, (s, i) => s*i);
            Console.WriteLine("\nLinqBegin15");
            Console.WriteLine(result15);

            /*
             LinqBegin18. Дана целочисленная последовательность. Извлечь из нее все четные отрицательные числа, поменяв порядок извлеченных чисел на обратный.
             */
            var intArray18 = new int[10] { -5, -8, 10, 0, -9, -6, -15, 17, 0, -2 };
            var result18 = intArray18.Where(i => i%2 == 0 && i < 0).Reverse();
            Console.WriteLine("\nLinqBegin18");
            Input(result18);

            /*
              LinqBegin19. Дана цифра D (целое однозначное число) и целочисленная последовательность A. Извлечь из A все различные положительные числа, 
              оканчивающиеся цифрой D (в исходном порядке). При наличии повторяющихся элементов удалять все их вхождения, кроме последних.
              Указание. Последовательно применить методы Reverse, Distinct, Reverse.
             */
            var d19 = 5;
            var intArray19 = new int[12] { -5, -8, 25, 0, -9, -6, -15, 17, 5, -2, 25, 5 };
            var result19 = intArray19.Where(i => i > 0 && int.Parse(i.ToString().Substring(i.ToString().Count() - 1, 1)) == d19).Reverse().Distinct().Reverse();
            Console.WriteLine("\nLinqBegin19");
            Input(result19);

            /*
             LinqBegin22. Дано целое число K (> 0) и строковая последовательность A. Строки последовательности содержат только цифры и заглавные буквы латинского алфавита. 
             Извлечь из A все строки длины K, оканчивающиеся цифрой, отсортировав их в лексикографическом порядке по возрастанию.
             */
            var k22 = 5;
            var a22 = new string[5]{"F1f89", "4f3dd", "Affffff", "Afff8", "Bfff3" };
            var result22_ = a22.Where(a => a.Length == k22 && a.Substring(a.Count()-1,1).Intersect("0123456789").Any()).OrderBy(s=>s);
            Console.WriteLine("\nLinqBegin22");
            Input(result22_);


            /*
             LinqBegin27. Дано целое число D и целочисленная последовательность A. 
             Начиная с первого элемента A, большего D, извлечь из A все нечетные положительные числа, поменяв порядок извлеченных чисел на обратный.
             */
            var d27 = 10;
            var intArray27 = new int[15] {0,  7, 5, -5, -8, 25, 0, -9, -6, -15, 17, 5, -2, 25, 5 };
            var result27 = intArray27.SkipWhile(i => i < d27).Where(i => i > 0 && i%2 != 0).Reverse();
            Console.WriteLine("\nLinqBegin27");
            Input(result27);

            /*
             LinqBegin32. Дана последовательность непустых строк A. Получить последовательность символов, каждый элемент которой является начальным символом 
             соответствующей строки из A. Порядок символов должен быть обратным по отношению к порядку элементов исходной последовательности.
             */
            var a32 = new string[8] { "asdfg", "aasda", "qwert", "jaa", "bcd", "a5a", "ccccccc", "a" };
            var result32_ = a32.Select(a => a.First()).Reverse();
            Console.WriteLine("\nLinqBegin32");
            Input(result32_);

            /*
             LinqBegin43. Дано целое число K (> 0) и последовательность непустых строк A. 
             Получить последовательность символов, которая определяется следующим образом: для первых K элементов последовательности A 
             в новую последовательность заносятся символы, стоящие на нечетных позициях
             данной строки (1, 3, …), а для остальных элементов A — символы на четных позициях (2, 4, …). 
             В полученной последовательности поменять порядок элементов на обратный.
             */
            var k43 = 2;
            var a43 = new string[8] { "asdfg", "aasda", "qwert", "jaa", "bcd", "a5a", "ccccccc", "a" };
            var result43_ = a43.Take(k43).SelectMany(s => s.Where((p, i) => i%2 != 0)).Concat(a43.Skip(k43).SelectMany(s => s.Where((p, i) => i % 2 == 0)));
            Console.WriteLine("\nLinqBegin43");
            Input(result43_);


            /*
             LinqBegin44. Даны целые числа K1 и K2 и целочисленные последовательности A и B. 
             Получить последовательность, содержащую все числа из A, большие K1, и все числа из B, меньшие K2. Отсортировать полученную последовательность по возрастанию.
             */
            var k1 = 2;
            var k2 = 5;
            var intArray441 = new int[15] { 0, 7, 5, -5, -8, 25, 0, -9, -6, -15, 17, 5, -2, 25, 5 };
            var intArray442 = new int[15] { 5, 15, -9, -5, 8, 100, 7, -45, 46, -1, 37, -5, 14, 2, 5 };
            var result44 = intArray441.Where(i => i > k1).Concat(intArray442.Where(i => i > k2));
            Console.WriteLine("\nLinqBegin44");
            Input(result44);


            /*
             LinqBegin46. Даны последовательности положительных целых чисел A и B; все числа в каждой последовательности различны. 
             Найти последовательность всех пар чисел, удовлетворяющих следующим условиям: первый элемент пары принадлежит последовательности A, 
             второй принадлежит B, и оба элемента оканчиваются одной и той же цифрой. 
             Результирующая последовательность называется внутренним объединением последовательностей A и B по ключу, 
             определяемому последними цифрами исходных чисел. Представить найденное объединение в виде последовательности строк, 
             содержащих первый и второй элементы пары, разделенные дефисом, например, «49-129». Порядок следования пар 
             должен определяться исходным порядком элементов последовательности A, а для равных первых элементов — порядком элементов последовательности B.
             */

            var a461 = new int[10] {5, 7, 99, 25, 15, 1, 29, 6, 70, 35};
            var a462 = new int[10] {6, 8, 19, 5, 12, 2, 50, 16, 30, 11};

            var result46 = a461.Zip(a462, (a1, a2) => new {a1, a2}).Where(a => a.a1.ToString()[a.a1.ToString().Count()-1] == a.a2.ToString()[a.a2.ToString().Count() - 1]);
            Console.WriteLine("\nLinqBegin46");
            Input(result46);


            /*
             LinqBegin56. Дана целочисленная последовательность A. Сгруппировать элементы последовательности A, оканчивающиеся одной и той же цифрой, 
             и на основе этой группировки получить последовательность строк вида «D:S», где D — ключ группировки 
             (т. е. некоторая цифра, которой оканчивается хотя бы одно из чисел последовательности A), а S — сумма всех чисел из A, которые оканчиваются цифрой D. 
             Полученную последовательность упорядочить по возрастанию ключей. Указание. Использовать метод GroupBy.
             */

            var a56 = new int[10] { 5, 7, 99, 25, 15, 1, 29, 6, 70, 35 };
            var result56 = a56.GroupBy(a => a.ToString()[a.ToString().Count() - 1]).OrderBy( a=> a.Key);
            Console.WriteLine("\nLinqBegin56");
            foreach (var line in result56)
            {
                Console.WriteLine("{0}:{1}", line.Key, line.Count());
            }


            /*
             LinqBegin57. Дана целочисленная последовательность. Среди всех элементов последовательности, оканчивающихся одной и той же цифрой, выбрать максимальный. 
             Полученную последовательность максимальных элементов упорядочить по возрастанию их последних цифр.
             */

            var a57 = new int[10] { 5, 7, 99, 25, 15, 1, 29, 6, 70, 35 };
            var result57 = a56.GroupBy(a => a.ToString()[a.ToString().Count() - 1]).OrderBy(a => a.Key);
            Console.WriteLine("\nLinqBegin57");
            foreach (var line in result57)
            {
                Console.WriteLine(line.Max());
            }

            /*
             LinqBegin60. Дана последовательность непустых строк A, содержащих только заглавные буквы латинского алфавита. 
             Для всех строк, начинающихся с одной и той же буквы, определить их суммарную длину и получить последовательность строк вида «S-C», 
             где S — суммарная длина всех строк из A, которые начинаются с буквы С. Полученную последовательность упорядочить по убыванию числовых значений сумм, 
             а при равных значениях сумм — по возрастанию кодов символов C.
           */

            var a60 = new string[8] {"ASDFERGD", "BAGRBR", "TREGKBJB", "ABARGURB", "PBRNBERB", "AVBABRBB", "BJRIJBJB", "QVRBJHRB"};
            var result60 = a60.GroupBy(data => data.First()).Select( group => new {key = group.Key, sum = group.Sum( current => current.Length )}).OrderByDescending(final => final.sum);
            Console.WriteLine("\nLinqBegin60");
            foreach (var line in result60)
            {
                Console.WriteLine("{0}-{1}", line.key, line.sum);
            }


            /*
             LinqObj85. Даны последовательности C, D и E, включающие следующие поля:
             C: <Название магазина> <Код потребителя> <Скидка (в процентах)> 
             D: <Артикул товара> <Цена (в рублях)> <Название магазина> 
             E: <Артикул товара> <Код потребителя> <Название магазина>
            Свойства последовательностей описаны в преамбуле к данной подгруппе заданий. Для каждой пары «потребитель–магазин», указанной в E, 
            определить суммарный размер скидок на все товары, приобретенные этим потребителем в данном магазине (вначале выводится код потребителя, затем название магазина, 
            затем суммарный размер скидки). При вычислении размера скидки на каждый приобретенный товар копейки отбрасываются. 
            Если потребитель приобретал товары в некотором магазине без скидки, то информация о соответствующей паре «потребитель–магазин» не выводится. 
            Если не найдено ни одной подходящей пары «потребитель–магазин», то записать в результирующий файл текст «Требуемые данные не найдены». 
            Сведения о каждой паре «потребитель–магазин» выводить на новой строке и упорядочивать по возрастанию кодов потребителей, а для одинаковых кодов — по названиям магазинов в алфавитном порядке.
             */
            var c85 = new[]  //Скидка для потребителей
            {
                new {name = "ATB",        potrCode = 1, discount = 5},
                new {name = "Marketopt",  potrCode = 1, discount = 2},
                new {name = "Marketopt",  potrCode = 2, discount = 3},
                new {name = "Optovichok", potrCode = 3, discount = 5},
                new {name = "Silpo",      potrCode = 3, discount = 6},
                new {name = "Trash",      potrCode = 3, discount = 7},
                new {name = "Silpo",      potrCode = 4, discount = 7},
                new {name = "Trash",      potrCode = 5, discount = 5},
                new {name = "Optovichok", potrCode = 5, discount = 1}
            };

            var d85 = new[]  //цена на товар
            {
                new {tovarCode = 1, price = 10, name = "ATB"},
                new {tovarCode = 1, price = 12, name = "Marketopt"},
                new {tovarCode = 2, price = 25, name = "Trash"},
                new {tovarCode = 3, price = 2 , name = "Optovichok"},
                new {tovarCode = 4, price = 70, name = "Silpo"},
                new {tovarCode = 5, price = 20, name = "Silpo"},
                new {tovarCode = 5, price = 21, name = "Trash"},
                new {tovarCode = 5, price = 22, name = "Optovichok"},
                new {tovarCode = 6, price = 5,  name = "Silpo"},
                new {tovarCode = 6, price = 5,  name = "ATB"}
            };

            var e85 = new[] // Покупки потребителей
            {
                new {tovarCode = 1, potrCode = 5, name = "ATB"},
                new {tovarCode = 1, potrCode = 2, name = "Marketopt"},
                new {tovarCode = 1, potrCode = 2, name = "ATB"},
                new {tovarCode = 2, potrCode = 1, name = "Trash"},
                new {tovarCode = 2, potrCode = 2, name = "Trash"},
                new {tovarCode = 3, potrCode = 5, name = "Optovichok"},
                new {tovarCode = 3, potrCode = 4, name = "Optovichok"},
                new {tovarCode = 4, potrCode = 4, name = "Silpo"},
                new {tovarCode = 4, potrCode = 5, name = "Silpo"},
                new {tovarCode = 5, potrCode = 1, name = "Silpo"},
                new {tovarCode = 5, potrCode = 2, name = "Trash"},
                new {tovarCode = 5, potrCode = 3, name = "Trash"},
                new {tovarCode = 2, potrCode = 3, name = "Trash"},
                new {tovarCode = 5, potrCode = 3, name = "Optovichok"},
                new {tovarCode = 6, potrCode = 4, name = "ATB"},
                new {tovarCode = 6, potrCode = 2, name = "Silpo"}
            };
            var result85 = e85.
                Join(d85, first => new {first.tovarCode, first.name}, second => new {second.tovarCode, second.name}, 
                    (first, second) => new {potrCode = first.potrCode, tovarCode = first.tovarCode, name = first.name, price = second.price}).
                Join(c85, first => new {first.potrCode, first.name}, second => new {second.potrCode, second.name},
                    (first, second) => new { potrCode = first.potrCode, tovarCode = first.tovarCode, name = first.name, price = first.price, discount = second.discount }).
                GroupBy(result => result.potrCode).Select(group => new {potrCode = group.First().potrCode, name = group.First().name, allDiscount = (int)group.Sum(current => current.discount * current.price * 0.01)});

            Console.WriteLine("\nLinqBegin85");
            Input(result85);

            //second variant
            var result851 =
                from e_ in e85
                join d_ in d85 on new {e_.tovarCode, e_.name} equals new {d_.tovarCode, d_.name}
                join c_ in c85 on new {e_.potrCode, e_.name} equals new {c_.potrCode, c_.name}
                select new {e_.potrCode, d_.name, d_.price, c_.discount}
                into j1
                group j1 by j1.potrCode
                into final
                select new
                {
                    potrCode = final.First().potrCode,
                    name = final.First().name,
                    allDiscount = (int) final.Sum(current => current.discount*current.price)
                };

            Input(result85);
        }



        public static void Input(IEnumerable data )
        {
            if (data == null)
                return;
            var i = 1;
            foreach (var d in data)
            {
                Console.WriteLine("{0}: {1}", i++, d);
            }
        }
    }
}
