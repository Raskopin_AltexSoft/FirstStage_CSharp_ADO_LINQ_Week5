﻿using System.Data.Entity;
using Task3.Entities;

namespace Task3.Context
{
    internal class BooksShopContext: DbContext
    {
        public BooksShopContext()
            : base("BooksShopContext")
        {
            
        }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<PublishingHouse> PublishingHouses { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
    }
}
