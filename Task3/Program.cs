﻿using System;
using System.Linq;
using Task3.Context;
using Task3.Entities;

namespace Task3
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //filing database
            //FillDb();
            
            using (var db = new BooksShopContext())
            {
                /*
                 1. Реализовать вставку строк в таблицу со столбцами, значение для которых
                создается автоматически или которые имеют значение по умолчанию.
                 */
                 // we create new publishing house without property "Publish" because it has a default value

                var newPublishingHouse = new PublishingHouse { City = "Poltava"};
                //db.PublishingHouses.Add(newPublishingHouse);


                /*
                 * 3. Реализовать добавление данных в таблицу с использованием значений из
                    другой таблицы
                 */

                var newBook = new Book() {Pages = 200, TitleBook = "avtobiografia"};
                var ph = db.PublishingHouses.FirstOrDefault(p => p.City == "Kremenchug");
                var author = db.Authors.Find(1);
                newBook.Author = author;
                newBook.PublishingHouse = ph;
                //db.Books.Add(newBook);

                /*
                 1. Реализуйте удаление всех строк в таблице воспользуйтесь инструкцией TRUNCATE TABLE
                 */
                //var author1 = db.Authors.FirstOrDefault();
                //db.Authors.RemoveRange(db.Authors);

                //db.Database.ExecuteSqlCommand("EXEC sp_MSforeachtable \"ALTER TABLE ? NOCHECK CONSTRAINT all\"");
                //db.Database.ExecuteSqlCommand("TRUNCATE TABLE Authors;");
                //db.Database.ExecuteSqlCommand("exec sp_MSforeachtable  "ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all\"");

                /*
                 2. Реализуйте удаление строк задав ограничения количества удаляемых строк
                    (предложение WHERE) c с указанием количества удаленных строк (функция @@ROWCOUNT)
                 */
                var rowCount = 3;
                var books = db.Books.Where(b => b.Author.NameAuthor == "Vladimir Nabokov").Take(rowCount);
                //db.Books.RemoveRange(books);


                /*
                 1. Реализуйте обновление нескольких столбцов
                 */

                foreach (var d in db.Deliveries)
                {
                    d.INN = 777;
                    d.Phone = "+380";
                }

                /*
                 2. Реализуйте обновление ограниченного количества строк (предложение WHERE), на которые влияет инструкция UPDATE
                 */
                var deliveriesWithSomeName = db.Deliveries.Where(d => d.NameCompany == "red");
                foreach (var d in deliveriesWithSomeName)
                {
                    d.INN = 777;
                    d.Phone = "+380";
                }

                /*
                 3. Реализуйте обновление ограниченного количества строк (предложение TOP), на которые влияет инструкция UPDATE
                 */

                var topTwoDeliveries = db.Deliveries.Take(2);
                foreach (var d in topTwoDeliveries)
                {
                    d.INN = 777;
                    d.Phone = "+380";
                }

                /*
                 8. Реализуйте обновление данных на основе данных из других таблиц
                 */
                var booksLev = db.Books.Where(b => b.Author.NameAuthor == "Lev Tolstoy");

                foreach (var book in booksLev)
                {
                    book.Author = db.Authors.FirstOrDefault(a => a.NameAuthor == "Lev Tolstoy");
                }

                db.SaveChanges();
            }
            Console.WriteLine("Job is done.");
            Console.ReadKey();
        }

        private static void FillDb()
        {
            using (var db = new BooksShopContext())
            {
                var d1 = new Delivery {NameDelivery = "first", Address = "first street", INN = 123, NameCompany = "red", Phone = "0033" };
                var d2 = new Delivery {NameDelivery = "second", Address = "second street", INN = 234, NameCompany = "green", Phone = "066" };
                var d3 = new Delivery {NameDelivery = "third", Address = "third street", INN = 345, NameCompany = "blue", Phone = "097" };

                db.Deliveries.Add(d1);
                db.Deliveries.Add(d2);
                db.Deliveries.Add(d3);

                var ph1 = new PublishingHouse { City = "Poltava", Publish = "Alfa" };
                var ph2 = new PublishingHouse { City = "Kremenchug", Publish = "Beta" };
                var ph3 = new PublishingHouse { City = "Kiev", Publish = "Omega" };

                db.PublishingHouses.Add(ph1);
                db.PublishingHouses.Add(ph2);
                db.PublishingHouses.Add(ph3);

                var tolstoy = new Author { NameAuthor = "Lev Tolstoy", Birthday = new DateTime(1847, 3, 26) };
                var dostoevskiy = new Author { NameAuthor = "Phedor Dostoevskiy", Birthday = new DateTime(1865, 8, 3) };
                var nabokov = new Author { NameAuthor = "Vladimir Nabokov", Birthday = new DateTime(1871, 9, 12) };
                var pushkin = new Author { NameAuthor = "Aleksandr Pushkin", Birthday = new DateTime(1799, 6, 2) };

                db.Authors.Add(tolstoy);
                db.Authors.Add(dostoevskiy);
                db.Authors.Add(nabokov);
                db.Authors.Add(pushkin);

                var book1 = new Book { TitleBook = "Voyna i mir", Author = tolstoy, Pages = 1500, PublishingHouse = ph1 };
                var book2 = new Book { TitleBook = "Anna Karenina", Author = tolstoy, Pages = 700, PublishingHouse = ph1 };
                var book3 = new Book { TitleBook = "Bratya Karamazovy", Author = dostoevskiy, Pages = 1200, PublishingHouse = ph1 };
                var book4 = new Book { TitleBook = "Idiot", Author = dostoevskiy, Pages = 600, PublishingHouse = ph2 };
                var book5 = new Book { TitleBook = "Besi", Author = dostoevskiy, Pages = 470, PublishingHouse = ph2 };
                var book6 = new Book { TitleBook = "Vesna v Fialte", Author = nabokov, Pages = 200, PublishingHouse = ph1 };
                var book7 = new Book { TitleBook = "Blednoe plamia", Author = nabokov, Pages = 600, PublishingHouse = ph2 };
                var book8 = new Book { TitleBook = "Priglashenie na kazn", Author = nabokov, Pages = 490, PublishingHouse = ph3 };
                var book9 = new Book { TitleBook = "Korol, dama, valet", Author = nabokov, Pages = 450, PublishingHouse = ph3 };
                var book10 = new Book { TitleBook = "Skazki", Author = pushkin, Pages = 500, PublishingHouse = ph2 };
                var book11 = new Book { TitleBook = "Ruslan i Ludmila", Author = pushkin, Pages = 300, PublishingHouse = ph3 };

                db.Books.Add(book1);
                db.Books.Add(book2);
                db.Books.Add(book3);
                db.Books.Add(book4);
                db.Books.Add(book5);
                db.Books.Add(book6);
                db.Books.Add(book7);
                db.Books.Add(book8);
                db.Books.Add(book9);
                db.Books.Add(book10);
                db.Books.Add(book11);

                var p1 = new Purchase { Book = book1, TypePurchase = "1", Amount = 1, Cost = 10, Delivery = d1, DateOrder = new DateTime(2016, 5, 5) };
                var p2 = new Purchase { Book = book1, TypePurchase = "2", Amount = 2, Cost = 20, Delivery = d2, DateOrder = new DateTime(2016, 4, 5) };
                var p3 = new Purchase { Book = book2, TypePurchase = "1", Amount = 3, Cost = 10, Delivery = d1, DateOrder = new DateTime(2016, 11, 28) };
                var p4 = new Purchase { Book = book3, TypePurchase = "2", Amount = 2, Cost = 40, Delivery = d2, DateOrder = new DateTime(2016, 1, 27) };
                var p5 = new Purchase { Book = book3, TypePurchase = "1", Amount = 3, Cost = 45, Delivery = d3, DateOrder = new DateTime(2016, 2, 19) };
                var p6 = new Purchase { Book = book4, TypePurchase = "2", Amount = 2, Cost = 56, Delivery = d1, DateOrder = new DateTime(2016, 2, 15) };
                var p7 = new Purchase { Book = book5, TypePurchase = "1", Amount = 1, Cost = 5, Delivery = d1, DateOrder = new DateTime(2016, 11, 11) };
                var p8 = new Purchase { Book = book5, TypePurchase = "2", Amount = 1, Cost = 25, Delivery = d3, DateOrder = new DateTime(2016, 7, 9) };
                var p9 = new Purchase { Book = book6, TypePurchase = "1", Amount = 1, Cost = 25, Delivery = d1, DateOrder = new DateTime(2016, 6, 7) };
                var p10 = new Purchase { Book = book7, TypePurchase = "2", Amount = 10, Cost = 17, Delivery = d1, DateOrder = new DateTime(2016, 7, 15) };
                var p11 = new Purchase { Book = book8, TypePurchase = "1", Amount = 5, Cost = 10, Delivery = d1, DateOrder = new DateTime(2016, 8, 28) };
                var p12 = new Purchase { Book = book8, TypePurchase = "2", Amount = 7, Cost = 10, Delivery = d2, DateOrder = new DateTime(2016, 9, 22) };
                var p13 = new Purchase { Book = book8, TypePurchase = "1", Amount = 7, Cost = 38, Delivery = d3, DateOrder = new DateTime(2016, 9, 12) };
                var p14 = new Purchase { Book = book9, TypePurchase = "2", Amount = 1, Cost = 4, Delivery = d2, DateOrder = new DateTime(2016, 9, 3) };
                var p15 = new Purchase { Book = book10, TypePurchase = "3", Amount = 2, Cost = 12, Delivery = d1, DateOrder = new DateTime(2016, 3, 23) };
                var p16 = new Purchase { Book = book10, TypePurchase = "3", Amount = 3, Cost = 45, Delivery = d2, DateOrder = new DateTime(2016, 2, 12) };
                var p17 = new Purchase { Book = book10, TypePurchase = "3", Amount = 12, Cost = 54, Delivery = d3, DateOrder = new DateTime(2016, 5, 5) };
                var p18 = new Purchase { Book = book11, TypePurchase = "2", Amount = 1, Cost = 15, Delivery = d1, DateOrder = new DateTime(2016, 5, 15) };
                var p19 = new Purchase { Book = book11, TypePurchase = "3", Amount = 1, Cost = 10, Delivery = d3, DateOrder = new DateTime(2016, 7, 12) };

                db.Purchases.Add(p1);
                db.Purchases.Add(p2);
                db.Purchases.Add(p3);
                db.Purchases.Add(p4);
                db.Purchases.Add(p5);
                db.Purchases.Add(p6);
                db.Purchases.Add(p7);
                db.Purchases.Add(p8);
                db.Purchases.Add(p9);
                db.Purchases.Add(p10);
                db.Purchases.Add(p11);
                db.Purchases.Add(p12);
                db.Purchases.Add(p13);
                db.Purchases.Add(p14);
                db.Purchases.Add(p15);
                db.Purchases.Add(p16);
                db.Purchases.Add(p17);
                db.Purchases.Add(p18);
                db.Purchases.Add(p19);

                db.SaveChanges();
            }
        }
    }
}
