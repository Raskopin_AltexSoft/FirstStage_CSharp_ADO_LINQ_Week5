﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Task3.Entities
{
    internal class Author
    {
        public Author()
        {
            Books = new List<Book>();
        }

        [Key]
        public int CodeAuthor { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string NameAuthor { get; set; }
        public DateTime Birthday { get; set; }

        //reverse relations one to many
        public virtual ICollection<Book> Books { get; set; }
    }
}
