﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Task3.Entities
{
    internal class Book
    {
        public Book()
        {
            Purchases = new List<Purchase>();
        }

        [Key]
        public int CodeBook { get; set; }
        [Required]
        public string TitleBook { get; set; }

        public short Pages { get; set; }

        //relations one to many
        public int CodeAuthor { get; set; }
        public virtual Author Author { get; set; }

        public int CodePublish { get; set; }
        public virtual PublishingHouse PublishingHouse { get; set; }

        //reverse relations one to many
        public virtual List<Purchase> Purchases { get; set; }
    }
}
