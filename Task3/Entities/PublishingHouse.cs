﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Task3.Entities
{
    internal class PublishingHouse
    {
        public PublishingHouse()
        {
            Books = new List<Book>();
            Publish = "newPublish";
        }

        [Key]
        public int CodePublish { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string Publish { get; set; }

        [StringLength(30, MinimumLength = 2)]
        public string City { get; set; }

        //reverse relations one to many
        public virtual List<Book> Books { get; set; }
    }
}
