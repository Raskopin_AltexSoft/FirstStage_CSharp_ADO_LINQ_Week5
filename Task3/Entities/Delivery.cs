﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Task3.Entities
{
    internal class Delivery
    {
        public Delivery()
        {
            Purchases = new List<Purchase>();
        }

        [Key]
        public int CodeDelivery { get; set; }
        [Required]
        public string NameDelivery { get; set; }
        public string NameCompany { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int INN { get; set; }

        //reverse relations one to many
        public virtual List<Purchase> Purchases { get; set; }
        
    }


}
