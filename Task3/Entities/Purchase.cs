﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Task3.Entities
{
    internal class Purchase
    {
        [Key]
        public int CodePurchase { get; set; }
        public DateTime DateOrder { get; set; }

        [Required]
        public string TypePurchase { get; set; }
        public uint Cost { get; set; }
        public ushort Amount { get; set; }

        //relations one to many
        public int CodeBook { get; set; }
        public virtual Book Book { get; set; }

        public int CodeDelivery { get; set; }
        public virtual Delivery Delivery { get; set; }
    }
}
