﻿using System;
using System.Data.SqlClient;


namespace Task1
{
    internal class DataBaseTask
    {
        private const string DataSource = @".\SQLEXPRESS";
        private const string InitialCatalog = "northwind";
        private const bool IntegratedSecurity = true;
        private SqlConnectionStringBuilder connect;
        private readonly string[] queries = new string[10];
        private readonly string[] views = new string[7];
        public DataBaseTask()
        {
            views[0] = @"
            CREATE VIEW T21  AS  
            SELECT p.ProductName as 'Товар', od.Quantity as 'Количество', od.UnitPrice as 'Цена', o.OrderDate as 'Дата' FROM Orders as o
	            JOIN [Order Details] as od ON od.OrderID = o.OrderID
	            JOIN Products as p ON p.ProductID = od.ProductID
            WHERE YEAR(o.OrderDate) < 1997
            ";
            views[1] = @"
            CREATE VIEW T22 WITH ENCRYPTION AS  
            SELECT top 7 p.ProductName as 'Товар', CAST((CAST(od.UnitPrice as FLOAT) - CAST(od.UnitPrice as FLOAT) * CAST(od.Discount as FLOAT))  * od.Quantity as decimal(38, 2)) as 'Сумма', o.OrderDate as 'Дата продажи', c.ContactName as 'Покупатель', , od.UnitPrice as 'Цена'
            FROM	Orders as o
	            JOIN [Order Details] as od ON od.OrderID = o.OrderID
	            JOIN Products as p ON p.ProductID = od.ProductID
	            JOIN Customers as c ON c.CustomerID = o.CustomerID;
            ";
            views[2] = @"
            CREATE VIEW T23  AS  
            SELECT ProductName as 'Товар', SUM(CAST((CAST(od.UnitPrice as FLOAT) - CAST(od.UnitPrice as FLOAT) * CAST(od.Discount as FLOAT))  * od.Quantity as decimal(38, 2))) as 'Сумма'
                FROM	Orders as o
	            JOIN [Order Details] as od ON od.OrderID = o.OrderID
	            JOIN Products as p ON p.ProductID = od.ProductID
	            JOIN Customers as c ON c.CustomerID = o.CustomerID
            GROUP BY p.ProductName;
            ";
            views[3] = @"
            CREATE VIEW T24  WITH ENCRYPTION AS  
            SELECT ProductName as 'Товар', SUM(od.Quantity) as 'Количество'
                FROM	Orders as o
	            JOIN [Order Details] as od ON od.OrderID = o.OrderID
	            JOIN Products as p ON p.ProductID = od.ProductID
	            JOIN Customers as c ON c.CustomerID = o.CustomerID
            GROUP BY p.ProductName;
            ";
            views[4] = @"
            CREATE VIEW T25  AS  
            SELECT ProductName as 'Товар', SUM(CAST((CAST(od.UnitPrice as FLOAT) - CAST(od.UnitPrice as FLOAT) * CAST(od.Discount as FLOAT))  * od.Quantity as decimal(38, 2))) as 'Сумма'
                FROM	Orders as o
	            JOIN [Order Details] as od ON od.OrderID = o.OrderID
	            JOIN Products as p ON p.ProductID = od.ProductID
	            JOIN Customers as c ON c.CustomerID = o.CustomerID
            WHERE YEAR(o.OrderDate) = 1997
            GROUP BY p.ProductName;
            ";
            views[5] = @"
            CREATE VIEW T26  AS  
            SELECT TOP 8 ProductName as 'Товар', SUM(CAST((CAST(od.UnitPrice as FLOAT) - CAST(od.UnitPrice as FLOAT) * CAST(od.Discount as FLOAT))  * od.Quantity as decimal(38, 2))) as 'Сумма'
                FROM	Orders as o
	            JOIN [Order Details] as od ON od.OrderID = o.OrderID
	            JOIN Products as p ON p.ProductID = od.ProductID
	            JOIN Customers as c ON c.CustomerID = o.CustomerID
            WHERE YEAR(o.OrderDate) = 1997
            GROUP BY p.ProductName;
            ";
            views[6] = @"
            CREATE VIEW T27  WITH ENCRYPTION AS  
            SELECT c.ContactName as 'Покупатель', SUM(CAST((CAST(od.UnitPrice as FLOAT) - CAST(od.UnitPrice as FLOAT) * CAST(od.Discount as FLOAT))  * od.Quantity as decimal(38, 2))) as 'Сумма' 
            FROM Customers as c 
	            JOIN Orders as o ON o.CustomerID = c.CustomerID
	            JOIN [Order Details] as od ON o.OrderID = od.OrderID
            GROUP BY c.ContactName
            HAVING SUM(CAST((CAST(od.UnitPrice as FLOAT) - CAST(od.UnitPrice as FLOAT) * CAST(od.Discount as FLOAT))  * od.Quantity as decimal(38, 2))) > 500;
            ";


            //    18.Используемая база данных Northwind.Написать скрипт на выборку всех записей о количестве продаж каждого товара, где количество> 10. (Детализация – товар, количество).Результат отсортирован по сумме, обратная сортировка.
            queries[0] =
                @"
                SELECT p.ProductName as 'Товар', od.Quantity as 'Количество' 
                FROM [Order Details] as od
                    JOIN Products as p ON p.ProductID = od.ProductID
                WHERE od.Quantity > 10
                ORDER BY (od.Quantity * od.UnitPrice) DESC";
            //    19.Используемая база данных Northwind.Написать скрипт на выборку всех записей о количестве покупок сделанных каждым покупателем, где количество < 5. (Детализация – покупатель, количество).Результат отсортирован по сумме, прямая сортировка.
            queries[1] =
                @"
                SELECT c.ContactName as 'Покупатель', Quantity as 'Количество' FROM Customers as c 
	                JOIN Orders as o ON o.CustomerID = c.CustomerID
	                JOIN [Order Details] as od ON o.OrderID = od.OrderID
                WHERE 
	                od.Quantity > 5
                ORDER BY (od.Quantity * od.UnitPrice) 
                ";
            //    20.Используемая база данных Northwind.Написать скрипт на выборку количества записей о покупках сделанных каждым покупателями, в имени которых присутствует хотя бы одна буква “a”. (Детализация – покупатель, количество). Результат отсортирован по сумме, прямая сортировка.
            queries[2] =
                @"
                SELECT c.ContactName as 'Покупатель', COUNT(c.ContactName) as 'Количество' FROM Customers as c 
	                JOIN Orders as o ON o.CustomerID = c.CustomerID
	                JOIN [Order Details] as od ON o.OrderID = od.OrderID
                WHERE CHARINDEX('c', c.ContactName)>0
                GROUP BY c.ContactName
                ORDER BY SUM(od.Quantity * od.UnitPrice)
                ";
            //    21.Используемая база данных Northwind.Создать представление на выборку записей о продажах, сделанных до 1997 года. (Детализация – какой товар, количество, цена продажи, дата продажи).Результат отсортирован по цене, обратная сортировка.
            queries[3] =
                @"
                SELECT * FROM T21 ORDER BY Цена DESC
                ";
            //    22.Используемая база данных Northwind.Создать представление на выборку первых 7 записей о продажах. (Детализация – какой товар, сумма, дата продажи, покупатель).Результат отсортирован по цене, прямая сортировка. Исходный текст представления зашифрован.
            queries[4] =
                @"
                SELECT * FROM T22 ORDER BY Цена;
                ";
            //    23.Используемая база данных Northwind.Создать представление на выборку всех записей о суммарных продажах каждого товара. (Детализация – какой товар, сумма).Результат отсортирован по сумме, обратная сортировка.
            queries[5] =
                @"
                SELECT * FROM T23 ORDER BY Сумма DESC;
                ";
            //    24.Используемая база данных Northwind.Создать представление на выборку всех записей о количестве продаж каждого товара. (Детализация – товар, количество).Результат отсортирован по сумме, обратная сортировка. Исходный текст представления зашифрован.
            queries[6] =
                @"
                SELECT * FROM T24 ORDER BY SUM(CAST((CAST(od.UnitPrice as FLOAT) - CAST(od.UnitPrice as FLOAT) * CAST(od.Discount as FLOAT))  * od.Quantity as decimal(38, 2))) DESC;
                ";
            //    25.Используемая база данных Northwind.Создать представление на выборку всех записей о суммарных продажах каждого товара за 1997 год. (Детализация – какой товар, сумма).Результат отсортирован по сумме, обратная сортировка.
            queries[7] =
                @"
                SELECT * FROM T25 ORDER BY Сумма DESC;
                ";
            //    26.Используемая база данных Northwind.Создать представление на выборку первых 8 записей о суммарных продажах каждого товара за 1997 год. (Детализация – какой товар, сумма).Результат отсортирован по сумме, обратная сортировка.
            queries[8] =
                @"
                SELECT * FROM T26 ORDER BY Сумма DESC;
                ";
            //    27.Используемая база данных Northwind.Создать представление на выборку всех записей о суммарных покупках каждого покупателя, на сумму большую 500. (Детализация – покупатель, сумма).Результат отсортирован по сумме, обратная сортировка. Исходный текст представления зашифрован.
            queries[9] =
                @"
                SELECT * FROM T27;
                ";
        
        }

        public void DbWork()
        {
            connect = new SqlConnectionStringBuilder();
            connect.DataSource = DataSource;
            connect.InitialCatalog = InitialCatalog;
            connect.IntegratedSecurity = IntegratedSecurity;

            using (var cn = new SqlConnection())
            {
                cn.ConnectionString = connect.ConnectionString;
                try
                {
                    foreach (var view in views)
                    {
                       // cn.Open();
                       // var command = new SqlCommand(view, cn);
                       // command.ExecuteNonQuery();
                       // cn.Close();
                    }
                    var i = 0;
                    foreach (var query in queries)
                    {
                        if (i == 4)
                        {
                            cn.Open();
                            var command = new SqlCommand(query, cn);
                            var reader = command.ExecuteReader();
                            OutputResult(reader);
                            cn.Close();
                        }
                        i++;
                    }
   
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    cn.Close();
                }
            }
        }

        private void OutputResult(SqlDataReader reader)
        {
            var counter = 1;
            while (reader.Read())
            {
                Console.Write("{0, -4}| ", counter++);
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    if (i == reader.FieldCount - 1)
                        Console.Write("{0}", reader[i]);
                    else
                        Console.Write("{0,-30} | ", reader[i]);
                }
                Console.WriteLine();
            }
        }
    }
}
